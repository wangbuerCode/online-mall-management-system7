package com.clothing.controller;

import com.clothing.pojo.Cloth;
import com.clothing.pojo.PageBean;
import com.clothing.pojo.User;
import com.clothing.service.ClothService;
import com.clothing.service.ClothSortService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@Controller
public class AdminClothController {
    @Autowired
    @Qualifier("ClothServiceImpl")
    private ClothService clothService;
    @Autowired
    @Qualifier("ClothSortServiceImpl")
    private ClothSortService clothSortService;
    @RequestMapping("/AdminQueryClothListA")
    private String  AdminQueryClothListA(HttpServletRequest request, String pageNumStr, String pageSizeStr, String clothName){
        if (StringUtils.isBlank(pageNumStr)){
            pageNumStr = "1";
        }
        if (StringUtils.isBlank(pageSizeStr)){
            pageSizeStr = "5";
        }
        if (clothName==null){
            clothName="";
        }
        int pageNum = Integer.parseInt(pageNumStr);
        int pageSize = Integer.parseInt(pageSizeStr);
        int totalCount = clothService.queryClothListCount(clothName);
        int totalPage=totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
        int index = (pageNum - 1) * pageSize;
        List<Cloth> clothList = clothService.queryClothListPG(index, pageSize, clothName);
        for (Cloth cloth : clothList) {
            cloth.setClothSortD(clothSortService.queryClothSortByID(Integer.parseInt(cloth.getClothSort())));
        }
        System.out.println(clothList);
        PageBean<Cloth> pageBean = new PageBean<Cloth>();
        pageBean.setPageSize(pageSize);
        pageBean.setPageNum(pageNum);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(clothList);
        pageBean.setTotalCount(totalCount);
        request.setAttribute("clothList",clothList);
        request.setAttribute("clothName",clothName);
        request.setAttribute("pb",pageBean);
        return "/clothes-table";
    }
    @RequestMapping("/deleteCloth")
    private String  deleteCloth(HttpServletRequest request, String pageNumStr, String pageSizeStr, String clothName,String clothID){
        clothService.deleteCloth(Integer.parseInt(clothID));
        if (StringUtils.isBlank(pageNumStr)){
            pageNumStr = "1";
        }
        if (StringUtils.isBlank(pageSizeStr)){
            pageSizeStr = "5";
        }
        if (clothName==null){
            clothName="";
        }
        int pageNum = Integer.parseInt(pageNumStr);
        int pageSize = Integer.parseInt(pageSizeStr);
        int totalCount = clothService.queryClothListCount(clothName);
        int totalPage=totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
        int index = (pageNum - 1) * pageSize;
        List<Cloth> clothList = clothService.queryClothListPG(index, pageSize, clothName);
        for (Cloth cloth : clothList) {
            cloth.setClothSortD(clothSortService.queryClothSortByID(Integer.parseInt(cloth.getClothSort())));
        }
        System.out.println(clothList);
        PageBean<Cloth> pageBean = new PageBean<Cloth>();
        pageBean.setPageSize(pageSize);
        pageBean.setPageNum(pageNum);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(clothList);
        pageBean.setTotalCount(totalCount);
        request.setAttribute("clothList",clothList);
        request.setAttribute("clothName",clothName);
        request.setAttribute("pb",pageBean);
        return "/clothes-table";
    }
    @RequestMapping("/queryClothByIDA")
    private String queryClothByIDA(String clothID,HttpServletRequest request){
        Cloth cloth = clothService.queryClothById(Integer.parseInt(clothID));
        request.setAttribute("clothU",cloth);
        return "/clothes-update";
    }
    @RequestMapping("/updateClothA")
    private String updateClothA(Cloth cloth,HttpServletRequest request){
        clothService.updateClothA(cloth);
        Cloth cloth1 = clothService.queryClothById(cloth.getClothID());
        request.setAttribute("clothU",cloth1);
        request.setAttribute("msgupd","修改成功");
        return "/clothes-update";
    }
    @RequestMapping("/addClothA")
    private String addClothA(Cloth cloth,HttpServletRequest request){

        String filename = (String) request.getSession().getAttribute("filename");
        cloth.setClothImg("img/"+filename);
        clothService.addClothA(cloth);
        request.setAttribute("msgadd","添加成功");
        return "/clothes-add";
    }

}
