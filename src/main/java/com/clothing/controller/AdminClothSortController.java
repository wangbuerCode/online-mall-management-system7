package com.clothing.controller;

import com.clothing.pojo.ClothSort;
import com.clothing.service.ClothSortService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@RestController
public class AdminClothSortController {
    @Autowired
    @Qualifier("ClothSortServiceImpl")
    private ClothSortService clothSortService;
    @RequestMapping("/queryALLClothSort")
    private String queryALLClothSort() throws JsonProcessingException {
        List<ClothSort> clothSorts = clothSortService.queryClothSortList();
        ObjectMapper objectMapper = new ObjectMapper();
        String val = objectMapper.writeValueAsString(clothSorts);
        return val;
    }

}
