package com.clothing.controller;

import com.clothing.pojo.Consume;
import com.clothing.pojo.PageBean;
import com.clothing.pojo.User;
import com.clothing.service.ClothService;
import com.clothing.service.ConsumeService;
import com.clothing.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@Controller
public class AdminConsumeController {
    @Autowired
    @Qualifier("ConsumeServiceImpl")
    private ConsumeService consumeService;
    @Autowired
    @Qualifier("userServiceImpl")
    private UserService userService;
    @Autowired
    @Qualifier("ClothServiceImpl")
    private ClothService clothService;
    @RequestMapping("/queryConsumeListA")
    private String queryConsumeListA(HttpServletRequest request, String pageNumStr, String pageSizeStr, String consumeID){
        if (StringUtils.isBlank(pageNumStr)){
            pageNumStr = "1";
        }
        if (StringUtils.isBlank(pageSizeStr)){
            pageSizeStr = "5";
        }
        int pageNum = Integer.parseInt(pageNumStr);
        int pageSize = Integer.parseInt(pageSizeStr);
        int totalCount = consumeService.queryConsumeListCount(consumeID);
        int totalPage=totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
        int index = (pageNum - 1) * pageSize;
        List<Consume> consumeList = consumeService.queryConsumeListA(index, pageSize, consumeID);
        for (Consume consume : consumeList) {
            consume.setUser(userService.queryUserByID(consume.getConsumeUID()));
            consume.setCloth(clothService.queryClothById(consume.getConsumeCID()));
        }
        PageBean<Consume> pageBean = new PageBean<Consume>();
        pageBean.setPageSize(pageSize);
        pageBean.setPageNum(pageNum);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(consumeList);
        pageBean.setTotalCount(totalCount);
        request.setAttribute("consumeList",consumeList);
        request.setAttribute("consumeID",consumeID);
        request.setAttribute("pb",pageBean);
        return "/user-shopping";
    }
    @RequestMapping("deleteConsumeA")
    private String deleteConsumeA(HttpServletRequest request, String pageNumStr, String pageSizeStr, String consumeID,String delConsumeID){
        consumeService.deleteConsume(Integer.parseInt(delConsumeID));
        if (StringUtils.isBlank(pageNumStr)){
            pageNumStr = "1";
        }
        if (StringUtils.isBlank(pageSizeStr)){
            pageSizeStr = "5";
        }
        int pageNum = Integer.parseInt(pageNumStr);
        int pageSize = Integer.parseInt(pageSizeStr);
        int totalCount = consumeService.queryConsumeListCount(consumeID);
        int totalPage=totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
        int index = (pageNum - 1) * pageSize;
        List<Consume> consumeList = consumeService.queryConsumeListA(index, pageSize, consumeID);
        for (Consume consume : consumeList) {
            consume.setUser(userService.queryUserByID(consume.getConsumeUID()));
            consume.setCloth(clothService.queryClothById(consume.getConsumeCID()));
        }
        PageBean<Consume> pageBean = new PageBean<Consume>();
        pageBean.setPageSize(pageSize);
        pageBean.setPageNum(pageNum);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(consumeList);
        pageBean.setTotalCount(totalCount);
        request.setAttribute("consumeList",consumeList);
        request.setAttribute("consumeID",consumeID);
        request.setAttribute("pb",pageBean);
        return "/user-shopping";
    }

}
