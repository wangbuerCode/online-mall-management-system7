package com.clothing.controller;

import com.clothing.dao.ClothMapper;
import com.clothing.pojo.Cloth;
import com.clothing.pojo.User;
import com.clothing.service.ClothService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@Controller
@RequestMapping("/cloth")
public class ClothController {
    @Autowired
    @Qualifier("ClothServiceImpl")
    private ClothService clothService;
    @RequestMapping("/queryCloth")
    public String queryCloth(String id,String startIndex,HttpServletRequest request) {
        Map map=new HashMap();
        String clothSort =(String) request.getSession().getAttribute("clothSort");
        if (id != null&&clothSort==null){
            map.put("clothSort",id);
        }else if (clothSort!=null){
            map.put("clothSort",clothSort);
        }
        if (startIndex == null){
            startIndex="0";
        }
        map.put("startIndex",Integer.parseInt(startIndex));
        map.put("pageSize",9);
        List<Cloth> clothList= clothService.queryCloth(map);
        int clothCount = clothService.queryClothCount(map);
        int clothTitle = clothCount / 9;
        request.getSession().setAttribute("clothSort",id);
        request.getSession().setAttribute("clothTitle",clothTitle);
        request.getSession().setAttribute("clothCount",clothCount);
        request.getSession().setAttribute("clothList",clothList);

        return "redirect:/shopping/html/category.jsp";
    }
    @RequestMapping("/queryClothById")
    public String queryClothById(String id,HttpServletRequest request){
        Cloth cloth = clothService.queryClothById(Integer.parseInt(id));
        request.getSession().setAttribute("clothByID",cloth);
        System.out.println(cloth);
        return "redirect:/shopping/html/detail.jsp";

    }
    @RequestMapping("/queryClothByName")
    public String queryClothByName(String clothName,HttpServletRequest request){
        String startIndex=null;
        Map map=new HashMap();
        String clothSort=null;
        String id=null;
        if (id != null&&clothSort==null){
            map.put("clothSort",id);
        }else if (clothSort!=null){
            map.put("clothSort",clothSort);
        }
        if (startIndex == null){
            startIndex="0";
        }
        map.put("startIndex",Integer.parseInt(startIndex));
        map.put("pageSize",9);
        List<Cloth> cloths = clothService.queryCloth(map);
        List<Cloth> clothList=new ArrayList<Cloth>();
        for (Cloth cloth : cloths) {
            if (cloth.getClothName().contains(clothName)){
                clothList.add(cloth);
            }
        }

        request.getSession().setAttribute("clothSort",id);
        request.getSession().setAttribute("clothTitle",null);
        request.getSession().setAttribute("clothCount",null);
        request.getSession().setAttribute("clothList",clothList);
        return "redirect:/shopping/html/category.jsp";
    }
}
