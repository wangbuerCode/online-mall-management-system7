package com.clothing.controller;

import com.clothing.pojo.Cloth;
import com.clothing.pojo.Consume;
import com.clothing.pojo.User;
import com.clothing.service.ClothService;
import com.clothing.service.ConsumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@Controller
@RequestMapping("/consume")
public class ConsumeController {
    @Autowired
    @Qualifier("ConsumeServiceImpl")
    private ConsumeService consumeService;
    @Autowired
    @Qualifier("ClothServiceImpl")
    private ClothService clothService;
    @RequestMapping("/queryConsume")
    public String queryConsume(HttpServletRequest request){
        User user =(User) request.getSession().getAttribute("user");
        List<Consume> consumeList = consumeService.queryConsume(user.getUserID());
        int sum=0;
        for (Consume consume : consumeList) {
            int consumeCID = consume.getConsumeCID();
            Cloth cloth = clothService.queryClothById(consumeCID);
            consume.setCloth(cloth);
            sum +=consume.getConsumeSum();
        }
        request.getSession().setAttribute("ConsumeClothList",consumeList);
        System.out.println(consumeList);
        request.getSession().setAttribute("sum",sum);
        return "redirect:/shopping/html/order.jsp";

    }
}
