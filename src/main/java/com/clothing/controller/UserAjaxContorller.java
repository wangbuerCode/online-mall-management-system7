package com.clothing.controller;

import com.clothing.pojo.User;
import com.clothing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@RestController

public class UserAjaxContorller {
    @Autowired
    @Qualifier("userServiceImpl")
    private UserService userService;

    @RequestMapping("/queryUserByName")
    public String queryUserByName(String userName) {
        User user = userService.queryUserByName(userName);
        System.out.println(user);
        String msg = "";
        if (user != null) {
            msg = "0";
        } else {
            msg = "1";
        }
        return msg;
    }

    @RequestMapping("/queryUserByEmail")
    public String queryUserByEmail(String Email,HttpServletRequest request) {
        User user = userService.queryUserByEmail(Email);
        request.getSession().setAttribute("userUpdate",user);
        String msg = "";
        if (user != null) {
            msg = "0";
        } else {
            msg = "1";
        }
        return msg;
    }

    @RequestMapping("/login")
    public String userLogin(String username, String password, HttpServletRequest request, Model model) {
        User user = userService.userLogin(username, password);
        String msg = "";
        if (user != null) {
            request.getSession().setAttribute("user", user);
            msg = "0";
        } else {
            msg = "1";
        }
        return msg;
    }
    @RequestMapping("/updatePassword")
    public String updatePassword(String updatePassword, HttpServletRequest request) {
        User userUpdate =(User) request.getSession().getAttribute("userUpdate");
        userUpdate.setUserPassword(updatePassword);
        int i = userService.updatePassword(userUpdate);
        String msg = "";
        if (i > 0) {
            msg = "0";
        } else {
            msg = "1";
        }
        return msg;
    }
}
