package com.clothing.controller;

import com.clothing.pojo.PageBean;
import com.clothing.pojo.User;
import com.clothing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.commons.lang3.StringUtils;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@Controller
public class AdminUserController {
    @Autowired
    @Qualifier("userServiceImpl")
    private UserService userService;
    @RequestMapping("/AdminQueryUserList")
    private String AdminQueryUserList(HttpServletRequest request,String pageNumStr,String pageSizeStr,String userRname){
        if (StringUtils.isBlank(pageNumStr)){
            pageNumStr = "1";
        }
        if (StringUtils.isBlank(pageSizeStr)){
            pageSizeStr = "5";
        }
        if (userRname==null){
            userRname="";
        }
        int pageNum = Integer.parseInt(pageNumStr);
        int pageSize = Integer.parseInt(pageSizeStr);
        int totalCount = userService.queryUserListCount(userRname);
        int totalPage=totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
        int index = (pageNum - 1) * pageSize;
        List<User> userList = userService.queryUserListPG(index, pageSize, userRname);
        PageBean<User> pageBean = new PageBean<User>();
        pageBean.setPageSize(pageSize);
        pageBean.setPageNum(pageNum);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(userList);
        pageBean.setTotalCount(totalCount);
        request.setAttribute("userList",userList);
        request.setAttribute("userRname",userRname);
        request.setAttribute("pb",pageBean);
        return "/user-table";
    }
    @RequestMapping("/updateUserA")
    private String updateUserA(HttpServletRequest request,String pageNumStr,String pageSizeStr,String userRname,String userPassword,String userSex,String userID){
        userService.updateUserA(userPassword,userSex,userID);
        if (StringUtils.isBlank(pageNumStr)){
            pageNumStr = "1";
        }
        if (StringUtils.isBlank(pageSizeStr)){
            pageSizeStr = "5";
        }
        if (userRname==null){
            userRname="";
        }
        int pageNum = Integer.parseInt(pageNumStr);
        int pageSize = Integer.parseInt(pageSizeStr);
        int totalCount = userService.queryUserListCount(userRname);
        int totalPage=totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
        int index = (pageNum - 1) * pageSize;
        List<User> userList = userService.queryUserListPG(index, pageSize, userRname);
        PageBean<User> pageBean = new PageBean<User>();
        pageBean.setPageSize(pageSize);
        pageBean.setPageNum(pageNum);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(userList);
        pageBean.setTotalCount(totalCount);
        request.setAttribute("userList",userList);
        request.setAttribute("userRname",userRname);
        request.setAttribute("pb",pageBean);
        return "/user-table";
    }
    @RequestMapping("/deleteUserByIDA")
    private String deleteUserByIDA(HttpServletRequest request,String pageNumStr,String pageSizeStr,String userRname,String userID){
        userService.deleteUserA(Integer.parseInt(userID));
        if (StringUtils.isBlank(pageNumStr)){
            pageNumStr = "1";
        }
        if (StringUtils.isBlank(pageSizeStr)){
            pageSizeStr = "5";
        }
        if (userRname==null){
            userRname="";
        }
        int pageNum = Integer.parseInt(pageNumStr);
        int pageSize = Integer.parseInt(pageSizeStr);
        int totalCount = userService.queryUserListCount(userRname);
        int totalPage=totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
        int index = (pageNum - 1) * pageSize;
        List<User> userList = userService.queryUserListPG(index, pageSize, userRname);
        PageBean<User> pageBean = new PageBean<User>();
        pageBean.setPageSize(pageSize);
        pageBean.setPageNum(pageNum);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(userList);
        pageBean.setTotalCount(totalCount);
        request.setAttribute("userList",userList);
        request.setAttribute("userRname",userRname);
        request.setAttribute("pb",pageBean);
        return "/user-table";
    }

}
