package com.clothing.controller;

import com.clothing.pojo.Cloth;
import com.clothing.pojo.Consume;
import com.clothing.pojo.User;
import com.clothing.service.ConsumeService;
import com.clothing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@RestController
public class ConsumeAjaxController {
    @Autowired
    @Qualifier("ConsumeServiceImpl")
    private ConsumeService consumeService;
    @Autowired
    @Qualifier("userServiceImpl")
    private UserService userService;
    @RequestMapping("/addConsume")
    private String addConsume(String consumeNum, String consumeSize, HttpServletRequest request){
        Map map = new HashMap();
        User user =(User) request.getSession().getAttribute("user");
        Cloth clothByID =(Cloth) request.getSession().getAttribute("clothByID");
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(date);
        map.put("consumeUID",user.getUserID());
        map.put("consumeCID",clothByID.getClothID());
        map.put("consumeNum",Integer.parseInt(consumeNum));
        map.put("consumeSize",consumeSize);
        map.put("consumeSum",(int)(Integer.parseInt(consumeNum)*clothByID.getClothPrice()));
        map.put("consumeDate",format);
        map.put("consumeState","1");
        int i = consumeService.addConsume(map);
        String msg ="";
        if (i>0){
            msg="0";
        }else {
            msg="1";
        }
        return msg;
    }
    @RequestMapping("/deleteConsume")
    public String deleteConsume(String consumeID){
        int i = consumeService.deleteConsume(Integer.parseInt(consumeID));
        String msg="";
        if (i>0){
            msg="0";
        }else {
            msg="1";
        }
        return msg;
    }
    @RequestMapping("/updateConsume")
    public String updateConsume(HttpServletRequest request){
        List<Consume> consumeClothList = (List<Consume>) request.getSession().getAttribute("ConsumeClothList");
        System.out.println(consumeClothList);
        int sum = (Integer) request.getSession().getAttribute("sum");
        System.out.println(sum);
        User user =(User) request.getSession().getAttribute("user");
        for (Consume consume : consumeClothList) {
            consumeService.updateConsume(consume.getConsumeID());
        }
        user.setUserConsume(sum);
        int i = userService.updateUserConsume(user);
        String msg="";
        if (i>0){
            msg="0";
        }else {
            msg="1";
        }
        return msg;
    }
}
