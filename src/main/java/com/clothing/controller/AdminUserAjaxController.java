package com.clothing.controller;

import com.clothing.pojo.User;
import com.clothing.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@RestController
public class AdminUserAjaxController {
    @Autowired
    @Qualifier("userServiceImpl")
    private UserService userService;
    @RequestMapping("/queryUserByIDA")
    private String queryUserByIDA(String userID) throws JsonProcessingException {
        User user = userService.queryUserByID(Integer.parseInt(userID));
        ObjectMapper objectMapper = new ObjectMapper();
        String val = objectMapper.writeValueAsString(user);
        return val;

    }
}
