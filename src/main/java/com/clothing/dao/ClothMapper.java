package com.clothing.dao;

import com.clothing.pojo.Cloth;
import com.clothing.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
public interface ClothMapper {
    List<Cloth> queryCloth(Map map);
    int queryClothCount(Map map);
    Cloth queryClothById(@Param("clothID") int id);
    List<Cloth> queryClothListPG(@Param("pageNumStr") int pageNumStr, @Param("pageSizeStr") int pageSizeStr, @Param("clothName") String clothName);
    int queryClothListCount(@Param("clothName") String clothName);
    int deleteCloth(@Param("clothID") int clothID);
    int updateClothA(Cloth cloth);
    int addClothA(Cloth cloth);
}
