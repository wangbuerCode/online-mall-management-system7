package com.clothing.dao;

import com.clothing.pojo.Consume;
import com.clothing.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
public interface ConsumeMapper {
    int addConsume(Map map);
    List<Consume> queryConsume(@Param("consumeUID") int consumeUID);
    int deleteConsume(@Param("consumeID") int consumeID);
    int updateConsume(@Param("consumeID") int consumeID);
    List<Consume> queryConsumeListA(@Param("pageNumStr") int pageNumStr, @Param("pageSizeStr") int pageSizeStr,@Param("consumeID") String consumeID);
    int queryConsumeListCount(@Param("consumeID") String consumeID);
}
