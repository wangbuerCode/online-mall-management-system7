package com.clothing.dao;

import com.clothing.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
public interface UserMapper {
    void userRegister(User user);
    User queryUserByName(@Param("userName") String userName);
    User queryUserByEmail(@Param("Email") String Email);
    User userLogin(@Param("userName") String username, @Param("userPassword") String password);
    int updatePassword(User user);
    int updateUserConsume(User user);
    List<User> queryUserList();
    int queryUserListCount(@Param("userRname") String userRname);
    List<User> queryUserListPG(@Param("pageNumStr") int pageNumStr,@Param("pageSizeStr") int pageSizeStr,@Param("userRname") String userRname);
    User queryUserByID(@Param("userID") int userID);
    int updateUserA(@Param("userPassword") String userPassword,@Param("userSex") String userSex,@Param("userID") String userID);
    int deleteUserA(@Param("userID") int userID);
}
