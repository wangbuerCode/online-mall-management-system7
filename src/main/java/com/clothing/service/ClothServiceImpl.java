package com.clothing.service;


import com.clothing.dao.ClothMapper;
import com.clothing.pojo.Cloth;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
public class ClothServiceImpl implements ClothService{
    private ClothMapper clothMapper;
    @Autowired
    public void setClothMapper(ClothMapper clothMapper) {
        this.clothMapper = clothMapper;
    }

    public  List<Cloth>  queryCloth(Map map) {
        return clothMapper.queryCloth(map);
    }

    public int queryClothCount(Map map) {
        return clothMapper.queryClothCount(map);
    }

    public Cloth queryClothById(int id) {
        return clothMapper.queryClothById(id);
    }

    public List<Cloth> queryClothListPG(int pageNumStr, int pageSizeStr, String clothName) {
        return clothMapper.queryClothListPG(pageNumStr,pageSizeStr,clothName);
    }

    public int queryClothListCount(String clothName) {
        return clothMapper.queryClothListCount(clothName);
    }

    public int deleteCloth(int clothID) {
        return clothMapper.deleteCloth(clothID);
    }

    public int updateClothA(Cloth cloth) {
        return clothMapper.updateClothA(cloth);
    }

    public int addClothA(Cloth cloth) {
        return clothMapper.addClothA(cloth);
    }
}
