package com.clothing.service;

import com.clothing.dao.UserMapper;
import com.clothing.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@Service
public class UserServiceImpl implements UserService {
    private UserMapper userMapper;
    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public void userRegister(User user) {
        userMapper.userRegister(user);
    }

    public User queryUserByName(String userName) {
        return userMapper.queryUserByName(userName);
    }

    public User queryUserByEmail(String Email) {
        return userMapper.queryUserByEmail(Email);
    }

    public User userLogin(String username, String password) {
        return userMapper.userLogin(username,password);
    }

    public int updatePassword(User user) {
        return userMapper.updatePassword(user);
    }

    public int updateUserConsume(User user) {
        return userMapper.updateUserConsume(user);
    }

    public List<User> queryUserList() {
        return userMapper.queryUserList();
    }

    public int queryUserListCount(String userRname) {
        return userMapper.queryUserListCount(userRname);
    }

    public List<User> queryUserListPG(int pageNumStr, int pageSizeStr, String userRname) {
        return userMapper.queryUserListPG(pageNumStr,pageSizeStr,userRname);
    }

    public User queryUserByID(int userId) {
        return userMapper.queryUserByID(userId);
    }

    public int updateUserA(String userPassword, String userSex, String userID) {
        return userMapper.updateUserA(userPassword,userSex,userID);
    }

    public int deleteUserA(int userID) {
        return userMapper.deleteUserA(userID);
    }


}
