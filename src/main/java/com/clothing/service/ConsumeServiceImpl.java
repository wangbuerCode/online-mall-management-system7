package com.clothing.service;

import com.clothing.dao.ClothMapper;
import com.clothing.dao.ConsumeMapper;
import com.clothing.pojo.Consume;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
public class ConsumeServiceImpl implements ConsumeService {
    private ConsumeMapper consumeMapper;
    @Autowired

    public void setConsumeMapper(ConsumeMapper consumeMapper) {
        this.consumeMapper = consumeMapper;
    }

    public int addConsume(Map map) {
        return consumeMapper.addConsume(map);
    }

    public List<Consume> queryConsume(int ConsumeUID) {
        return consumeMapper.queryConsume(ConsumeUID);
    }

    public int deleteConsume(int consumeID) {
        return consumeMapper.deleteConsume(consumeID);
    }

    public int updateConsume(int consumeID) {
       return consumeMapper.updateConsume(consumeID);
    }

    public List<Consume> queryConsumeListA(int pageNumStr, int pageSizeStr, String consumeID) {
        return consumeMapper.queryConsumeListA(pageNumStr,pageSizeStr,consumeID);
    }

    public int queryConsumeListCount(String consumeID) {
        return consumeMapper.queryConsumeListCount(consumeID);
    }
}
