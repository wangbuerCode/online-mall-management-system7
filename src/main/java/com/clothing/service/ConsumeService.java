package com.clothing.service;

import com.clothing.pojo.Consume;
import org.apache.ibatis.annotations.Param;


import java.util.List;
import java.util.Map;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
public interface ConsumeService {
    int addConsume(Map map);
    List<Consume> queryConsume(int ConsumeUID);
    int deleteConsume(int consumeID);
    int updateConsume(int consumeID);
    List<Consume> queryConsumeListA(@Param("pageNumStr") int pageNumStr, @Param("pageSizeStr") int pageSizeStr, @Param("consumeID") String consumeID);
    int queryConsumeListCount(@Param("consumeID") String consumeID);
}
