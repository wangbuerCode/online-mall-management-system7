package com.clothing.service;

import com.clothing.dao.ClothSortMapper;
import com.clothing.pojo.ClothSort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@Service
public class ClothSortServiceImpl implements ClothSortService{
    private ClothSortMapper clothSortMapper;

    @Autowired
    public void setClothSortMapper(ClothSortMapper clothSortMapper) {
        this.clothSortMapper = clothSortMapper;
    }

    public List<ClothSort> queryClothSortList() {
        return clothSortMapper.queryClothSortList();
    }

    public ClothSort queryClothSortByID(int clothSortID) {
        return clothSortMapper.queryClothSortByID(clothSortID);
    }
}
