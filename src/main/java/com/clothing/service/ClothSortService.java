package com.clothing.service;

import com.clothing.pojo.ClothSort;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
public interface ClothSortService {
    List<ClothSort> queryClothSortList();
    ClothSort queryClothSortByID(@Param("clothSortID") int clothSortID);
}
