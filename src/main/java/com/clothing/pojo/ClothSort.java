package com.clothing.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClothSort {
    private int clothSortID;
    private String clothSortName;
}
