package com.clothing.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
//衣服实体类
public class Cloth {
    private int clothID;  //id
    private String clothName; //名字
    private int clothPrice; //价格
    private String clothSort; //类别
    private String clothImg;//照片
    private ClothSort clothSortD;
}
