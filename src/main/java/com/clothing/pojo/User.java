package com.clothing.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private int userID;//用户ID
    private String userName;//账户名
    private String userPassword;//密码
    private String userRname;//用户真实姓名
    private String userSex;//用户性别
    private String userEmail;//用户邮箱
    private int userConsume;//用户消费总金额

}
