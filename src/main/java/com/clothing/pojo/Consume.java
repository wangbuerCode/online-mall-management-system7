package com.clothing.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
/**
 * @author 是一个鸽子啊
 * @date 2020/4/14
 * @qq 364826415
 */
//购物车实体类
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Consume {
    private int consumeID;//订单ID
    private int consumeUID;//用户ID
    private int consumeCID;//衣服ID
    private int consumeNum;//订单数量
    private String consumeSize;//订单尺码
    private int consumeSum;//订单总金额
    private Date consumeDate;//订单时间
    private String consumeState;//订单状态
    private Cloth cloth;
    private User user;

}
